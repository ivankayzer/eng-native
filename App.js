import React from 'react';
import { WebView } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <WebView
        source={{uri: 'http://eng.kayzer.me/'}}
        style={{marginTop: 20}}
      />
    );
  }
}
